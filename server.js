const Telegraf = require("telegraf");
const fetch = require('node-fetch');
const mongoose = require("mongoose");
const Log = require("./log");
const Humanoid = require("humanoid-js");

require('dotenv').config();


var cse = process.env.GOOGLE_CSE_ID;
var googleApi = process.env.GOOGLE_SEARCH_API;
var telegramApi = process.env.LYRICS_API_TOKEN;
var dbUrl = process.env.DATABASE_URL;

mongoose.connect(dbUrl);

const bot = new Telegraf(telegramApi);
let humanoid = new Humanoid();

bot.use((ctx, next) => {
    const start = new Date()
    return next(ctx).then(() => {
        const ms = new Date() - start
        var newLog = new Log({
            date: new Date(),
            query: ctx.message,
        });
        newLog.save();
        console.log('Response time %sms', ms)
    });
});

bot.start((ctx) => ctx.reply('Welcome to ET Lyrics Bot! Search lyrics by using the artist\'s name and the song\'s title!'));
bot.help((ctx) => ctx.reply('Search lyrics by using the artist\'s name and the song\'s title!'));

bot.on('text', (ctx) => {
    getLyrics(ctx);
});

bot.on("inline_query", async ctx => {
	var query = ctx.inlineQuery.query.trim();
	if(query.length < 4) return;
	query = query.split(" ")
	console.log(query)
    	var req_url = `https://www.googleapis.com/customsearch/v1?q=[${query}]&cx=${cse}&num=10&key=${googleApi}&alt=json`;
    	fetch(req_url)
        .then(res => res.json())
        .then(json => {
            if(json.searchInformation?.totalResults == '0') {
                console.log("no results");
		return;
            }
            var res_url = json.items[0].link;
            humanoid.get(res_url)
                .then(async res =>  {
                    let body = res.body
                    var lyrics = body;
                    var singer = lyrics.split(/\<h2\>*.*\<b\>/)[1]
                    singer = singer.split("Lyrics</b></a></h2>")[0]
                    var title = lyrics.split("<b>")[2]
                    title = title.split("</b>")[0] + "\n";
                      var up_partition = '<!-- Usage of azlyrics.com content by any third-party lyrics provider is prohibited by our licensing agreement. Sorry about that. -->'
                    var down_partition = '<!-- MxM banner -->'
                    lyrics = singer + " - " + title + lyrics.split(up_partition)[1]
                    lyrics = lyrics.split(down_partition)[0]
                    lyrics = lyrics.replace(/<br\/>/gi, '').replace(/<br>/gi, '').replace(/<\/br>/gi, '')
                        .replace(/<i>/gi, '').replace(/<\/i>/gi, '').replace(/<\/div>/gi, '').trim()
                    
		recipes = [{id: 1, title: title, type: 'article', message_text: lyrics, description: singer, url: res_url}]
		return await ctx.answerInlineQuery(recipes);
  }).catch(console.log);
  }).catch(err => {
            console.error(err)
           // ctx.reply("Oops... Error Occured! Tell Sam he messed up :) @tar_xzvf");
            var newLog = new Log({
                date: new Date(),
                query: err,
            });
            newLog.save();
        });
});

bot.on("chosen_inline_result", ({ chosenInlineResult }) => {
	console.log("chosen inline result", chosenInlineResult);
});
bot.startPolling()

var getLyrics = (ctx) => {
    var query = ctx.message.text.trim().split(" ");
    var req_url = `https://www.googleapis.com/customsearch/v1?q=[${query}]&cx=${cse}&num=10&key=${googleApi}&alt=json`;
    fetch(req_url)
        .then(res => res.json())
        .then(json => {
	    if(json.searchInformation?.totalResults == '0') {
	    	ctx.reply("Sorry! No results found.");
		return;
	    }
            var res_url = json.items[0].link;
            humanoid.get(res_url)
                .then(res =>  {
		    let body = res.body
                    var lyrics = body;
                    var singer = lyrics.split(/\<h2\>*.*\<b\>/)[1]
                    singer = singer.split("Lyrics</b></a></h2>")[0]
                    var title = lyrics.split("<b>")[2]
                    title = title.split("</b>")[0] + "\n"
                    var up_partition = '<!-- Usage of azlyrics.com content by any third-party lyrics provider is prohibited by our licensing agreement. Sorry about that. -->'
                    var down_partition = '<!-- MxM banner -->'
                    lyrics = singer + " - " + title + lyrics.split(up_partition)[1]
                    lyrics = lyrics.split(down_partition)[0]
                    lyrics = lyrics.replace(/<br\/>/gi, '').replace(/<br>/gi, '').replace(/<\/br>/gi, '')
                        .replace(/<i>/gi, '').replace(/<\/i>/gi, '').replace(/<\/div>/gi, '').trim()
                    ctx.reply(lyrics);
                }).catch(err => { ctx.reply(err.message) });
        }).catch(err => {
            console.error(err)
	    ctx.reply("Oops... Error Occured! Tell Sam he messed up :) @tar_xzvf");
            var newLog = new Log({
                date: new Date(),
                query: err,
            });
            newLog.save();
        });
}
